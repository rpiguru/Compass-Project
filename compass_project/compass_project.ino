#include <Wire.h>
#include <HMC5883L.h>
 
HMC5883L compass;

float criteria_degree;
float previousDegree;
float off_degree = 10;
float margin = 4.0;
float headingDegrees;

const int on_off_switch = 4;
const int set_button = 3;
int set_button_state = HIGH;

const int relay_1 = 10;
const int relay_2 = 11;

bool b_started = false;
 
void setup()
{
  initialize_pins();
  
  Serial.begin(9600);
 
  // Initialize HMC5883L
  Serial.println("Initialize HMC5883L");
  while (!compass.begin())
  {
    Serial.println("Failed to start HMC5883L, please check wiring...");
    delay(500);
  }
 
  compass.setRange(HMC5883L_RANGE_1_3GA);
 
  // Setting mode
  compass.setMeasurementMode(HMC5883L_CONTINOUS);
 
  // Setting frequency measurements
  compass.setDataRate(HMC5883L_DATARATE_15HZ);
 
  // The number of samples averaged
  compass.setSamples(HMC5883L_SAMPLES_4);

  b_started = false;
}

void initialize_pins(){
  pinMode(on_off_switch, INPUT);
  pinMode(set_button, INPUT);
  pinMode(relay_1, OUTPUT);
  pinMode(relay_2, OUTPUT);

  digitalWrite(on_off_switch, HIGH); // Apply PULL-UP resistor
  digitalWrite(set_button, HIGH); // Apply PULL-UP resistor
  
  digitalWrite(relay_1, LOW);
  digitalWrite(relay_2, LOW);
}

void loop()
{
  if (digitalRead(on_off_switch) == HIGH){  // get status of ON/OFF switch
    get_heading_degree();
    if (digitalRead(set_button) == LOW && set_button_state == HIGH){  // when SET button is triggered from high to low
      b_started = true;
      criteria_degree = headingDegrees;
      Serial.print(" Setting criteria as ");
      Serial.println(criteria_degree);
    }
    else if (b_started == true){
      float offset = headingDegrees - criteria_degree;
      if (offset < 0) // add 360 to simplify calculation
          offset += 360;
      if (offset < off_degree){
        digitalWrite(relay_1, LOW);
        digitalWrite(relay_2, LOW);
      }
      else if (offset < 180){
            Serial.println("Turning relay 1 on, relay 2 off...");
            digitalWrite(relay_1, HIGH);
            digitalWrite(relay_2, LOW);
          }
      else {
        Serial.println("Turning relay 1 off, relay 2 on...");
        digitalWrite(relay_1, LOW);
        digitalWrite(relay_2, HIGH);
      }
    }
  }
  else{
    digitalWrite(relay_1, LOW);
    digitalWrite(relay_2, LOW);
  }

  set_button_state = digitalRead(set_button);
  delay(100);
}

void get_heading_degree(){
  Vector norm = compass.readNormalize();
 
  // Calculate direction (rad)
  float heading = atan2(norm.YAxis, norm.XAxis);
  
//  // Setting the declination angle Bytom 1'53E (positive) - United Arab Emirate.
//  // Formula: (deg + (min / 60.0)) / (180 / M_PI);
//  // Get proper deg value from http://www.magnetic-declination.com
//  float declinationAngle = (1.0 + (53.0 / 60.0)) / (180 / M_PI);
//  heading += declinationAngle;
 
  // Correct angle
  if (heading < 0)
  {
    heading += 2 * PI;
  }
 
  if (heading > 2 * PI)
  {
    heading -= 2 * PI;
  }
 
  // Conversion of radians to degrees
  headingDegrees = heading * 180/M_PI;
 
  // Output
  Serial.print(" Degress = ");
  Serial.print(headingDegrees);
  Serial.println();
}

