## BOM(Bill of Materials)

- Arduino UNO ($3.44)
	
	http://www.ebay.com/itm/NEW-UNO-R3-ATmega328P-CH340-Mini-USB-Board-for-Compatible-Arduino-SGHS-/172213084842?hash=item2818b322aa:g:OOIAAOSw9a5XPnGH

- HMC5883L ($2.59)

	http://www.ebay.com/itm/3V-5V-HMC5883L-Triple-Axis-Compass-Magnetometer-Sensor-Module-For-Arduino-/272041588532?hash=item3f56f13734:g:si8AAOSwMTZWREV2

- Sainsmart 2-way relay board ($4.45)
	
	http://www.ebay.com/itm/SainSmart-2-Channel-5V-Relay-Module-for-Arduino-Raspberry-Pi-ARM-PIC-AVR-DSP-/381636986262?hash=item58db564596:g:GtAAAOSwYlJW6dQf

- Toggle Switch ($ 0.299)
	
	http://www.ebay.com/itm/10pcs-12V-Car-Round-Rocker-Dot-Boat-Blue-LED-Light-Toggle-ON-OFF-Switch-/131670999198?hash=item1ea834009e:g:oEUAAOSwnH1WZqN~&vxp=mtr

- Push Button ($ 0.41)
	
	http://www.ebay.com/itm/6Pcs-Mini-12mm-Waterproof-Momentary-ON-OFF-Push-Button-Round-Switch-/201538126654?hash=item2eec9bc33e:g:PAEAAOSwvgdW4C4V

## Wiring components.

To see diagram, we need Fritzing application.
Please download Fritizing from http://fritzing.org and open the **schematic.fzz** with it.

You can see breadboard view and also schematic view as well.

### Arduino - HMC5883L

| **Arduino**  | **HMC5885L**   |
| ----         |:-------:       |
| GND          | GND            |
| 5V           | VCC            |
| A4(SDA)      | SDA			|
| A5(SCL)      | SCL		    |


### Arduino - 2 way relay

| **Arduino**  | **2 way relay**|
| ----         |:-------:       |
| GND          | GND            |
| 5V           | VCC            |
| D10	       | IN1			|
| D11	       | IN2		    |

### Arduino - Switch & Button
Please refer wiring diagram for switch and button.

In my diagram, I used LEDs instead of 2-way relay.

To replace it, just remove 2 LEDs and its pull-down resistors and connect Arduino's pin D10, D11 to the corresponding pins of relay, connect 5V, GND pin to the VCC, GND pin of relay.

## Writing firmware to Arduino

### Install Arduino IDE
Download latest version of Arduino IDE and install on your PC.
 
### Prepare library.
- Download zip file from https://github.com/jarzebski/Arduino-HMC5883L

- Open the Arduino IDE and click **Sketch/Include Library/Add .ZIP Library**, and select zip file downloaded above.
	
	Arduino IDE will say that HMC5883 library is successfully include.

### Write firmware.

Open the source file and upload it to Arduino.